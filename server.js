var lessMiddleware = require('less-middleware');
//setup Dependencies
var connect = require('connect')
    , express = require('express')
    , mail = require('nodemailer')
    , path = require('path')
    , io = require('socket.io')
    , port = (process.env.PORT || 666);

//Setup Express
var server = express.createServer();
server.configure(function(){
    server.set('views', __dirname + '/views');
    server.set('view options', { layout: false });
    server.use(connect.bodyParser());
    server.use(express.cookieParser());
    server.use(express.session({ secret: "shhhhhhhhh!"}));
    server.use(lessMiddleware(path.join(__dirname, 'public','css'),{

        dest: path.join(__dirname, 'public' ),
        debug:true
    }, {
        paths : [path.join(__dirname, 'bower_components','bootstrap','less')]
    } ));
    server.use(connect.static(__dirname + '/public', { maxAge: 315500000 }));
    server.use(server.router);
});

//setup the errors
server.error(function(err, req, res, next){
    if (err instanceof NotFound) {
        res.render('404.jade', { locals: { 
                  title : '404 - Not Found'
                 ,description: ''
                 ,author: ''
                 ,analyticssiteid: 'XXXXXXX' 
                },status: 404 });
    } else {
        res.render('500.jade', { locals: { 
                  title : 'The Server Encountered an Error'
                 ,description: ''
                 ,author: ''
                 ,analyticssiteid: 'XXXXXXX'
                 ,error: err 
                },status: 500 });
    }
});
server.listen( port);


///////////////////////////////////////////
//              Routes                   //
///////////////////////////////////////////

/////// ADD ALL YOUR ROUTES HERE  /////////

server.get('/', function(req,res){
  res.render('index.jade', {
    locals : { 
              title : 'Стіна киян'
             ,description: 'Унікальна стіна киян, складена з тисяч фото'
            }
  });
});
server.get('/:page',  function(req,res){

    res.render(req.params.page+'.jade',{
        locals : {
            title : 'Ціни і оплата'
            ,description: 'замовити'
        }
    })
});

var smtpTransport = mail.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "aboklogov@gmail.com",
        pass: "boklogov"
    }
});
server.post('/contact',function(req,res){
    var name = req.body.username;
    var email =req.body.email;
    var mailOptions = {
        from: "FACEWALL<"+ email + ">", // sender address
        to: "aboklogov@gmail.com, "+email, // list of receivers
        subject: "FaceWall питання від " + name, // Subject line
        text: req.body.txt + " " + email, // plaintext body
        //html: "<b>Hello world ✔</b>" // html body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            res.redirect('/');
            //console.log("Message sent: " + response.message);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });

});


//A Route for Creating a 500 Error (Useful to keep around)
server.get('/500', function(req, res){
    throw new Error('This is a 500 Error');
});

//The 404 Route (ALWAYS Keep this as the last route)
server.get('/*', function(req, res){
    throw new NotFound;
});

function NotFound(msg){
    this.name = 'NotFound';
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
}


console.log('Listening on http://0.0.0.0:' + port );
