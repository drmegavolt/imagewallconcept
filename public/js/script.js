/* Author: YOUR NAME HERE
 */


$(document).ready(function() {
    $('.btn-next').click(function(){
        if (!$('.check_term:checked').length){
            alert('Ви не погодились з умовами сервісу, нажміть галочку');
            return false;

        }

        if (!$('.photo_loader').val()){
            alert('Ви не вибрали фото для завантаження');
            return false;
        }
    });
    $('#slider1').tinycarousel();
    var mX = 800, mY=400, zX= 2, zY = 20;
    //x y w h type
    // 1 = perimeterZone
    var types = {
        1 : {cls:'perimeterZone'},
        2 : {cls:'bronzeZone'},
        3 : {cls:'silverZone'},
        4 : {cls:'goldZone'}
    };

    $('.positionSelector').on('click', '.available', function(){
        $('.positionSelector div').removeClass('selected');
        $(this).addClass('selected');
        console.log(this);
    });

    $('.zoneSelector').on('click',  function(){
        var ctx = $(this).closest('.box');
        var maxX = 40, maxY = 14 , size = 21;
        var isLocked = function(x,y){
            return ((x*x+y*x)%40);
        }
        if(ctx.hasClass('bronze')){
            isLocked = function(x,y){
                return ((y*y+y*x)%40);
            }
        }
        if(ctx.hasClass('vip')){
            isLocked = function(x,y){
                return ((y*x*y+y*x)%40);
            }
        }


        for(var x = 0 ; x<maxX; x++){
            for(var y = 0 ; y<maxY; y++){
                $('<div/>',{
                    css:{
                        position: 'absolute',
                        top: y*size + 'px' ,
                        left:maxX*size - x*size +'px'
                    },
                    'data-available' : true,
                    'class': isLocked(x,y)?'available':'locked'
                }).html(' ').appendTo('.positionSelector');
            }
        }
        //$('.positionSelector')
        $('.placeSelector').modal();
    });

    $('.box').mouseenter( function(){
        var cls = $(this).attr('class').substr(4);
        $('.wallPartSelector .'+cls).addClass('hovered');
    }).mouseleave( function(){
            var cls = $(this).attr('class').substr(4);
            $('.wallPartSelector .'+cls).removeClass('hovered');
    });
});

//  var socket = io.connect();
//
//  $('#sender').bind('click', function() {
//   socket.emit('message', 'Message Sent on ' + new Date());
//  });
//
//  socket.on('server_message', function(data){
//   $('#receiver').append('<li>' + data + '</li>');
//  });